<?php
/**
 * Created by PhpStorm.
 * User: ayen
 * Date: 2/16/16
 * Time: 6:06 PM
 */
function _node_theme_config() {
  $form['node_theme_debug'] = array(
    '#type' => 'checkbox',
    '#title' => 'Enable debug',
    '#default_value' => variable_get('node_theme_debug', 0),
  );

  return system_settings_form($form);
}
function _node_theme_manage() {
  $node_themes = _node_theme_get_entries();
  $form['add_entry'] = array(
    '#type' => 'submit',
    '#value' => '+ Add',
  );
  $form['table_start'] = array(
    '#markup' => '<table><tr><th>Path</th><th>Theme</th><th></th><th></th></tr>',
  );
  foreach ($node_themes as $path => $entry) {
    $rawpath = str_replace('/', '|', $path);
    $edit_link = '<a href="' . 'node_theme/edit/' . $rawpath . '/' . $entry->theme . '">Edit</a>';
    $delete_link = '<a href="' . 'node_theme/delete/' . $rawpath . '/' . $entry->theme . '">Delete</a>';
    $form['node_theme_' . $path] = array(
      '#markup' => '<tr><td>' . $entry->path . '</td><td>' . $entry->theme . '</td><td>' . $edit_link . '</td><td>' . $delete_link . '</td></tr>',
    );
  }
  $form['table_end'] = array(
    '#markup' => '</table>',
  );

  return $form;
}

function _node_theme_manage_submit($form_id, &$form_state) {
  if ($form_state['clicked_button']['#value'] == '+ Add') {
    drupal_goto('admin/config/node_theme/create');
  }

}

function _node_theme_create($form_id, &$form_state) {
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Relative (to the Drupal root) path with no leading /'),
    '#required' => TRUE,
  );
  $form['theme'] = array(
    '#type' => 'select',
    '#title' => t('Theme'),
    '#options' => array('' => 'Please select') + _node_theme_get_theme_list(),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

function _node_theme_create_validate($form_id, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Save' && substr($form_state['values']['path'], 0, 1) == '/') {
    form_set_error('path', 'There should not be a leading slash');
  }
  $ret = db_select('node_theme', 'nt')
    ->fields('nt')
    ->condition('path', $form_state['values']['path'])
    ->execute()
    ->fetchAllAssoc('path');
  if ($ret) {
    form_set_error('path', 'This a node_theme entry for this path already exists');
  }
}

function _node_theme_create_submit($form_id, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Save') {
    db_insert('node_theme')
      ->fields(array(
        'path' => $form_state['values']['path'],
        'theme' => $form_state['values']['theme']
      ))
      ->execute();
  }
  drupal_goto('admin/config/node_theme');
}

function _node_theme_update($form_id, &$form_state, $path, $theme) {
  $path = str_replace('|', '/', $path);
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => $path,
    '#description' => t('Relative (to the Drupal root) path with no leading /'),
    '#required' => TRUE,
  );
    $form['theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#options' => _node_theme_get_theme_list(),
      '#default_value' => $theme,
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
    );

  return $form;
}

function _node_theme_update_validate($form_id, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Save' && substr($form_state['values']['path'], 0, 1) == '/') {
    form_set_error('path', 'There should not be a leading slash');
  }
}

function _node_theme_update_submit($form_id, &$form_state) {
  if ($form_state['clicked_button']['#value'] == 'Save') {
    $original_path = str_replace('|', '/', $form_state['build_info']['args'][0]);
    $original_theme = $form_state['build_info']['args'][1];
    // There is a possibility that Save was clicked with no changes - no need to update the db then
    if ($original_path != $form_state['values']['path'] || $original_theme != $form_state['values']['theme']) {
      db_update('node_theme')
        ->fields(array(
          // The path might now be different than what is in the db, so use the path from the original form
          'path' => $form_state['values']['path'],
          'theme' => $form_state['values']['theme']
        ))
        ->condition('path', $original_path)
        ->execute();
    }
  }
  drupal_goto('admin/config/node_theme');
}

function _node_theme_delete($form_id, &$form_state, $path) {
  $path = str_replace('|', '/', $path);
  $form['markup'] = array(
    '#markup' => '<h2>Delete the entry for the path: ' . $path . ' ?</h2>',
  );
  $form['path'] = array(
    '#type' => 'hidden',
    '#value' => $path,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

function _node_theme_delete_submit($form_id, &$form_state) {
  $path = $form_state['values']['path'];
  if ($form_state['clicked_button']['#value'] == 'Delete') {
    db_delete('node_theme')
      ->condition('path', $path)
      ->execute();
  }
  drupal_goto('admin/config/node_theme');
}

function _node_theme_get_entries() {
  $result = db_select('node_theme', 'nt')
      ->fields('nt')
      ->orderBy('path')
      ->execute()
      ->fetchAllAssoc('path');
  return $result ? $result : array();
}