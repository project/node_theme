Welcome to Node Theme.

Node Theme is used to enable switching to another theme based on the path.
It is not only for nodes, but the name is carried over from D6.

Enabling Node Theme for the first time will add a table node_theme to your
database. This table is used to match paths and the desired theme. You can
select a theme for a node on the node form, or you can create and maintain
any path on the Node Theme manage page (admin/config/node_theme/manage).

If a page is requested by a raw path that does not exist in the table,
Node Theme will check for the alias path, if any, in the table. If the page
request is an alias that does not exist in the table, Node Theme will check
the table for the raw path. If no entry is found, or if the theme mapped to
the path is not active, Node Theme will do nothing. Otherwise, it will
present the requested theme as a custom theme, which switches the display
to that theme.